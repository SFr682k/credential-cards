%\iffalse
%<*package>
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%</package>
%\fi
%\iffalse
% Doc-Source file to use with LaTeX2e
% Copyright (C) 2018 Sebastian Friedl
%
% This work is subject to the LaTeX Project Public License, Version 1.3c or -- at
% your option -- any later version of this license.
% The work consists of the files
% credential-cards.cls
%
% This work has the LPPL maintenance status 'maintained'.
% Current maintainer of the work is Sebastian Friedl.
%\fi
%
%
%
% \subsubsection*{Initialization}
% \changes{1.0}{??}{Initial release}
% Identify the class and require \LaTeXe
%    \begin{macrocode}
\ProvidesClass{credential-cards}%
    [2018/09/15 (in development) Print multiple credentials per page]
\NeedsTeXFormat{LaTeX2e}
%    \end{macrocode}
%
%
%
% \subsubsection*{Require and configure stuff}
% Require "beamer" as the base class and remove those nasty navigation symbols:
%    \begin{macrocode}
\LoadClass{beamer}
\setbeamertemplate{navigation symbols}{}
%    \end{macrocode}
%
% "beamer" already requires the "geometry" package, but to be on the safe side \dots \\
% Since the frame sizes offered by "beamer" are quite small, change the page size to A4:
%    \begin{macrocode}
\RequirePackage{geometry}
\geometry{a4paper}
%    \end{macrocode}
%
% \medskip
% We also have to load \dots
% \begin{description}\itemsep0pt
%     \item[\texttt{environ}] for enabling enhanced handling of environments,
%     \item[\texttt{ifthen}]  for comparing counter values,
%     \item[\texttt{pifont}]  to obtain access on Dingbats symbols and
%     \item[\texttt{tikz}]    for placing the credential cards.
% \end{description}
%    \begin{macrocode}
\RequirePackage{environ}
\RequirePackage{ifthen}
\RequirePackage{pifont}
\RequirePackage{tikz}
%    \end{macrocode}
%
%
%
% \subsubsection*{Define and initiate variables}
% Define some counters to save cache size, number of cards allowed per page,
% number of cards already placed on the currently flushed page, total number
% of placed cards as well as a temporary counter:
%    \begin{macrocode}
\newcount\credcards@cachedCards
\newcount\credcards@cardsperpage
\newcount\credcards@placedstuff
\newcount\credcards@totalcards
\newcount\credcards@temp%
%    \end{macrocode}
%

%
% Also, define some \LaTeX\ conditionals to check whether some commands are used inside
% the preamble or the "document" environment:
%    \begin{macrocode}
\newif\if@credcards@inpreamble
\@credcards@inpreambletrue
\newif\if@credcards@indocument
\@credcards@indocumentfalse
%    \end{macrocode}
%
%
%
% \subsubsection*{Configuration}
% \textbf{TODO:} Some configuration stuff: \\
% Show/Hide ID \\
% Sepline \& ID box styles \\
% Number of columns \\
% Number of rows
%
%
%
% \subsubsection*{Ti\emph{k}Z styles}
%
%    \begin{macrocode}
\tikzstyle{sepline} = [dashed]
\tikzstyle{idbox}   = [font={\ttfamily\footnotesize}]
%    \end{macrocode}
%
%
%
% \subsubsection*{The card content}
% \DescribeEnv{credcardcontent}
% Provide a "credcardcontent" environment for defining the content of the credential cards:
%    \begin{macrocode}
\NewEnviron{credcardcontent}{%
    \if@credcards@inpreamble%
        \global\let\credcards@boxcontent\BODY%
    \else%
        \ClassError{credential-cards}%
            {credcardscontent has to be set in the preamble}%
            {You can't use the credcardscontent environment outside the preamble}%
    \fi%
}
%    \end{macrocode}
%
% \begin{macro}{\credcards@boxcontent}
% Print a placeholder text displayed on the credential cards until a \enquote{custom} content
% has been defined via the "credcardcontent" environment:
%    \begin{macrocode}
\def\credcards@boxcontent{\begin{minipage}[c][.225\textheight]{\textwidth}
    \textbf{Change the text of the single cards
            using the \texttt{credcardcontent} environment} \\
    \textit{This is an example depicting the available commands
            for printing data. \textbf{No credentials are inserted.}}
    
    \bigskip\medskip
    \begin{tabular}{@{}ll}
        \multicolumn{2}{@{}l}{\texttt{\textbackslash firstname}
                              \texttt{\textbackslash lastname},
                              Group \texttt{\textbackslash group}} \\[\smallskipamount]
        User name: & \texttt{\textbackslash username} \\
        Password:  & \texttt{\textbackslash password}
    \end{tabular}%
\end{minipage}}
%    \end{macrocode}
% \end{macro}
%
%
%
% \subsubsection*{Card IDs}
% Define internal counters for hour and minute, since they are not provided by \TeX\ itself:
%    \begin{macrocode}
\newcount\credcards@hour\relax
\credcards@hour=\time\relax
\divide\credcards@hour by 60\relax
\newcount\credcards@minute\relax
\credcards@minute=\credcards@hour\relax
\multiply\credcards@minute by -60\relax
\advance\credcards@minute by \time\relax
%    \end{macrocode}
%
% \begin{macro}{\credcards@timestamp}
% Remember when the compilation started:
%    \begin{macrocode}
\edef\credcards@timestamp{%
    \the\year%
    \ifnum\the\month<10{0}\fi\the\month%
    \ifnum\the\day<10{0}\fi\the\day%
    \,%
    \ifnum\the\credcards@hour<10{0}\fi\the\credcards@hour%
    \ifnum\the\credcards@minute<10{0}\fi\the\credcards@minute%
}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\credcards@printid}
% Prints and ID consisting of the current card's number and the compilation time.
%    \begin{macrocode}
\def\credcards@printid{%
    \global\advance\credcards@totalcards by 1\relax%
    \underline{%
        \ifnum\credcards@totalcards<100000{0}\fi%
        \ifnum\credcards@totalcards<010000{0}\fi%
        \ifnum\credcards@totalcards<001000{0}\fi%
        \ifnum\credcards@totalcards<000100{0}\fi%
        \ifnum\credcards@totalcards<000010{0}\fi%
        \the\credcards@totalcards%
    }/\credcards@timestamp%
}
%    \end{macrocode}
% \end{macro}
%
%
%
% \subsubsection*{Set up cache and variable management}
% \begin{macro}{\credcards@clearCache}
% Provide an internal macro for clearing the cache:
%    \begin{macrocode}
\def\credcards@clearCache{%
    \credcards@temp=0\relax%
    \whiledo{\credcards@temp < \credcards@cardsperpage}{%
        \advance\credcards@temp by 1\relax%
        \expandafter\def\csname @credcards@cardcache@fn@\romannumeral\credcards@temp\endcsname{}%
        \expandafter\def\csname @credcards@cardcache@ln@\romannumeral\credcards@temp\endcsname{}%
        \expandafter\def\csname @credcards@cardcache@gp@\romannumeral\credcards@temp\endcsname{}%
        \expandafter\def\csname @credcards@cardcache@un@\romannumeral\credcards@temp\endcsname{}%
        \expandafter\def\csname @credcards@cardcache@pw@\romannumeral\credcards@temp\endcsname{}%
    }
    \credcards@cachedCards=0\relax%
}
%    \end{macrocode}
% \end{macro}
%
%
%
% \subsubsection*{Page layout}
% The page layout is defined as the "frame"'s background canvas
% and constructed inside a "tikzpicture":
%    \begin{macrocode}
\defbeamertemplate*{background canvas}{credcards}{\begin{tikzpicture}
%    \end{macrocode}
%
% \textit{Step 1:}\quad Clip the "tikzpicture"'s size to the "frame"'s size:
%    \begin{macrocode}
    \clip (-.5\paperwidth,0) rectangle (.5\paperwidth,-\paperheight);
%    \end{macrocode}
%
% \textit{Step 2:}\quad Draw the separation lines, including a cute little scissors symbol:
%    \begin{macrocode}
    \credcards@placedstuff=1\relax% intended
    \whiledo{\credcards@placedstuff < \credcards@cardsperpage}{%
        \draw[sepline]
            (-.5\paperwidth, -{\the\credcards@placedstuff%
                               /\credcards@cardsperpage*\paperheight})
            -- ++(\paperwidth, 0);
        \node[right=.5cm, inner sep=0pt, font=\Large, fill=white]
            at (-.5\paperwidth, -{\the\credcards@placedstuff%
                                  /\credcards@cardsperpage*\paperheight})
            {\ding{34}};
        \advance\credcards@placedstuff by 1\relax}
%    \end{macrocode}
%
% \textit{Step 3:}\quad Print the card IDs
%    \begin{macrocode}
    \credcards@placedstuff=0\relax
    \whiledo{\credcards@placedstuff < \credcards@cardsperpage}{%
        \node[below left=5pt, idbox]
            at (.5\paperwidth, -{\the\credcards@placedstuff%
                                 /\credcards@cardsperpage)*\paperheight})
            {\credcards@printid};
        \advance\credcards@placedstuff by 1\relax}
%    \end{macrocode}
%
% \textit{Step 4:}\quad Fetch the cached data and print the actual content
%    \begin{macrocode}
    \foreach \x in {1,2,...,\credcards@cardsperpage}
        \node at (0, -{(2*\x-1)/(2*\credcards@cardsperpage)*\paperheight}) {%
            \ifnum\x > \the\credcards@cachedCards%
                \edef\firstname{-/-}%
                \edef\lastname{-/-}%
                \edef\group{-/-}%
                \edef\username{-/-}%
                \edef\password{-/-}%
            \else%
                \edef\firstname{\csname @credcards@cardcache@fn@\romannumeral\x\endcsname}%
                \edef\lastname{\csname @credcards@cardcache@ln@\romannumeral\x\endcsname}%
                \edef\group{\csname @credcards@cardcache@gp@\romannumeral\x\endcsname}%
                \edef\username{\csname @credcards@cardcache@un@\romannumeral\x\endcsname}%
                \edef\password{\csname @credcards@cardcache@pw@\romannumeral\x\endcsname}%
            \fi%
            %
            \begin{minipage}[.225\textheight]{\textwidth}%
                \credcards@boxcontent%
            \end{minipage}};
\end{tikzpicture}}
%    \end{macrocode}
%
%
%
% \subsubsection*{Actions on \texttt{\textbackslash begin\{document\}}}
% Save current date and time to a \Lua\ variable (required for creating the IDs), clear the card cache and
% initialize the number of printed cards. \\
% \textbf{TODO:} Calculate the number of cards fitting on a page
%    \begin{macrocode}
\AtBeginDocument{%
    \@credcards@inpreamblefalse%
    \@credcards@indocumenttrue%
    \credcards@cardsperpage=5\relax%
    %
    \credcards@clearCache%
}
%    \end{macrocode}
%
%
%
% \subsubsection*{Printing credential cards}
% \begin{macro}{\printcredentials}
% Credentials are entered via the "\printcredentials" command. \\
% Entered credentials get cached. If the number of cached credentials is sufficient to print them
% onto a page, the whole cache gets flushed.
%    \begin{macrocode}
\def\printcredentials#1#2#3#4#5{%
    \if@credcards@indocument%
        \advance\credcards@cachedCards by 1\relax%
        \expandafter\edef\csname @credcards@cardcache@fn@\romannumeral\credcards@cachedCards\endcsname{#2}%
        \expandafter\edef\csname @credcards@cardcache@ln@\romannumeral\credcards@cachedCards\endcsname{#1}%
        \expandafter\edef\csname @credcards@cardcache@gp@\romannumeral\credcards@cachedCards\endcsname{#3}%
        \expandafter\edef\csname @credcards@cardcache@un@\romannumeral\credcards@cachedCards\endcsname{#4}%
        \expandafter\edef\csname @credcards@cardcache@pw@\romannumeral\credcards@cachedCards\endcsname{#5}%
        %
        \ifnum\credcards@cachedCards=\credcards@cardsperpage%
            \credcards@flushcredcardpage%
        \fi%
    \else%
        \ClassError{credential-cards}%
            {Use \noexpand\printcredentials only inside a document}%
            {You can't use \noexpand\printcredentials\space outside the
             document environment}
    \fi%
}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\credcards@flushcredcardpage}
% Flushes the cache by creating a "frame" (values from the cache are inserted by the backgroud template)
% and clearing the cache:
%    \begin{macrocode}
\def\credcards@flushcredcardpage{%
    \frame{}%
    \credcards@clearCache%
}
%    \end{macrocode}
% \end{macro}
%
%
%
% \subsubsection*{Actions on \texttt{\textbackslash end\{document\}}}
% Flush a last credential page if there are cached credentials:
%    \begin{macrocode}
\AtEndDocument{%
    \ifnum\credcards@cachedCards > 0%
        \credcards@flushcredcardpage%
    \fi%
    %
    \@credcards@indocumentfalse%
}
%    \end{macrocode}
%
%
%
% \subsubsection*{Famous last words}
% Enough code for that class. "\endinput".
%    \begin{macrocode}
\endinput
%    \end{macrocode}